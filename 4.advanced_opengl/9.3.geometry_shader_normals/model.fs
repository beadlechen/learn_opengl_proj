#version 330 core
out vec4 FragColor;

in vec2 texCoords;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_height1;

void main()
{
    vec4 diffuse = texture(texture_diffuse1, texCoords);
    vec4 height = texture(texture_height1, texCoords);
    FragColor = vec4(diffuse.rgb * height.r, 1.0);
}

