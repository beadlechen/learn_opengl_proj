﻿//
// Created by captain on 2021/4/6.
// 图片加载与解析
//
#include "texture2d.h"
#include "stb/stb_image.h"

using namespace std;

Texture2D* Texture2D::LoadFromFile(const std::string& image_file_path)
{
    Texture2D* texture2d = new Texture2D();
    //翻转图片，解析出来的图片数据从左下角开始，这是因为OpenGL的纹理坐标起始点为左下角
//    stbi_set_flip_vertically_on_load(true);
    int channels_in_file;//通道数
    unsigned char* data = stbi_load(image_file_path.c_str(),
                                    &(texture2d->width_), &(texture2d->height_), &channels_in_file, 0);
    if (data == nullptr)
    {
        cout << "Failed to load image file: " << image_file_path << endl;
        return nullptr;
    }

    //根据颜色通道数，判断颜色格式。
    switch (channels_in_file) {
        case 1:
        {
            texture2d->gl_texture_format_ = GL_ALPHA;
            break;
        }
        case 3:
        {
            texture2d->gl_texture_format_ = GL_RGB;
            break;
        }
        case 4:
        {
            texture2d->gl_texture_format_ = GL_RGBA;
            break;
        }
    }

    //通知显卡创建纹理对象，返回句柄;
    glGenTextures(1, &(texture2d->gl_texture_id_));

    //将纹理绑定到特定纹理目标;
    glBindTexture(GL_TEXTURE_2D, texture2d->gl_texture_id_);

    GLenum format = texture2d->gl_texture_format_;

    // 指定纹理环绕方式
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);

    //指定放大，缩小滤波方式，线性滤波，即放大缩小的插值方式;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); //GL_LINEAR
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //将图片数据上传到GPU;
    glTexImage2D(GL_TEXTURE_2D, 0, format, texture2d->width_, texture2d->height_, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    //释放图片文件内存
    stbi_image_free(data);

    return texture2d;
}
