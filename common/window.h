//
// Created by Administrator on 2024/3/15.
//

#ifndef WINDOW_H
#define WINDOW_H

#include <glad/gl.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <map>

#include "camera.h"

using namespace std;

class Window
{
public:
    Window()
    {
        glfwInit();
        // 设置OpenGL版本为3.3
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        // 设置OpenGL配置文件，使用核心模式
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
//    glfwWindowHint(GLFW_SAMPLES, 4);
#ifdef __APPLE__
        // 设置OpenGL配置文件，使用前向兼容模式（Mac OS X系统需要设置）
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    }
//    ~Window();

public:
    GLFWwindow* glfwWin = NULL;
    int width = 800;
    int height = 600;
    float deltaTime = 0.0f;
    Camera camera;

    bool create()
    {
        if (glfwWin != NULL)
        {
            cout << "Window already created" << std::endl;
            return false;
        }

        // 创建窗口对象
        glfwWin = glfwCreateWindow(width, height, "LearnOpenGL", NULL, NULL);
        if (glfwWin == NULL)
        {
            cout << "Failed to create GLFW window" << std::endl;
            glfwTerminate();
            return false;
        }
        glfwMakeContextCurrent(glfwWin);
        glfwSetFramebufferSizeCallback(glfwWin, framebuffer_size_callback);

        glfwSetCursorPosCallback(glfwWin, mouse_callback);
        glfwSetScrollCallback(glfwWin, scroll_callback);
        // tell GLFW to capture our mouse
        glfwSetInputMode(glfwWin, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
//    glfwSwapInterval(0);

        // glad: load all OpenGL function pointers
        // ---------------------------------------
        if (!gladLoadGL(glfwGetProcAddress))
        {
            cout << "Failed to initialize GLAD" << std::endl;
            return false;
        }

        camera = Camera(glm::vec3(0.0f, 0.0f, 3.0f));

        windowMap[glfwWin] = this;

        return true;
    }

    // 回调函数传参
    void update()
    {
        // per-frame time logic
        // --------------------
        float currentFrame = static_cast<float>(glfwGetTime());
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        process_input();
        show_fps();
    }

    glm::mat4 getProjection()
    {
        return glm::perspective(glm::radians(camera.Zoom), (float)width / (float)height, 0.1f, 100.0f);
    }

    glm::mat4 getProjection(float near, float far)
    {
        return glm::perspective(glm::radians(camera.Zoom), (float)width / (float)height, near, far);
    }

    void onProcessInput(void (*func)(Window* window))
    {
        on_process = func;
    }

private:
    float lastX = width / 2.0f;
    float lastY = height / 2.0f;

    float lastFrame;
    void (*on_process)(Window* window) = NULL;
    bool pressedAlt = false;
    bool firstMouse = true;

    static std::map<GLFWwindow*, Window*> windowMap;

    static void framebuffer_size_callback(GLFWwindow* window, int width, int height)
    {
        // make sure the viewport matches the new window dimensions; note that width and
        // height will be significantly larger than specified on retina displays.
        glViewport(0, 0, width, height);
    }

    static void mouse_callback(GLFWwindow* glfwWin, double xposIn, double yposIn)
    {
        Window *window = windowMap[glfwWin];

        // if alt is pressed, do not process mouse movement
        if (window->pressedAlt)
        {
            return;
        }

        float xpos = static_cast<float>(xposIn);
        float ypos = static_cast<float>(yposIn);

        if (window->firstMouse)
        {
            window->lastX = xpos;
            window->lastY = ypos;
            window->firstMouse = false;
        }

        float xoffset = xpos - window->lastX;
        float yoffset = window->lastY - ypos; // reversed since y-coordinates go from bottom to top

        window->lastX = xpos;
        window->lastY = ypos;

        window->camera.ProcessMouseMovement(xoffset, yoffset);
    }

    static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
    {
        windowMap[window]->camera.ProcessMouseScroll(static_cast<float>(yoffset));
    }

    void show_fps()
    {
        static double previous_seconds = 0.0;
        static int frame_count;
        double current_seconds = glfwGetTime();
        double elapsed_seconds = current_seconds - previous_seconds;
        if (elapsed_seconds > 0.25)
        {
            previous_seconds = current_seconds;
            double fps = static_cast<double>(frame_count) / elapsed_seconds;
            char tmp[128];
            sprintf(tmp, "opengl @ fps: %.2f", fps);
            // 后缀带上相机位置
            glm::vec3 pos = camera.Position;
            sprintf(tmp, "%s, [%.2f, %.2f, %.2f]", tmp, pos.x, pos.y, pos.z);
            sprintf(tmp, "%s, [%.2f, %.2f]", tmp, camera.Yaw, camera.Pitch);
            glfwSetWindowTitle(glfwWin, tmp);
            frame_count = 0;
        }
        frame_count++;
    }

    void process_input()
    {
        if(glfwGetKey(glfwWin, GLFW_KEY_ESCAPE) == GLFW_PRESS)
            glfwSetWindowShouldClose(glfwWin, true);

        camera.MovementSpeed = glfwGetKey(glfwWin, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS ? 2.5 * 10 : 2.5;

        if (glfwGetKey(glfwWin, GLFW_KEY_W) == GLFW_PRESS)
            camera.ProcessKeyboard(FORWARD, deltaTime);
        if (glfwGetKey(glfwWin, GLFW_KEY_S) == GLFW_PRESS)
            camera.ProcessKeyboard(BACKWARD, deltaTime);
        if (glfwGetKey(glfwWin, GLFW_KEY_A) == GLFW_PRESS)
            camera.ProcessKeyboard(LEFT, deltaTime);
        if (glfwGetKey(glfwWin, GLFW_KEY_D) == GLFW_PRESS)
            camera.ProcessKeyboard(RIGHT, deltaTime);

        // press 'alt' to enable mouse view
        if (glfwGetKey(glfwWin, GLFW_KEY_LEFT_ALT) == GLFW_PRESS && !pressedAlt)
        {
            pressedAlt = true;
            firstMouse = true;// 重新计算鼠标位置，避免突变
            glfwSetInputMode(glfwWin, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
        else if (glfwGetKey(glfwWin, GLFW_KEY_LEFT_ALT) == GLFW_RELEASE && pressedAlt)
        {
            pressedAlt = false;
            glfwSetInputMode(glfwWin, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }

        if (on_process != NULL)
        {
            on_process(this);
        }
    }
};

std::map<GLFWwindow*, Window*> Window::windowMap;

#endif
