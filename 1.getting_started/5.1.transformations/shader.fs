#version 330 core

in vec3 ourColor;
in vec2 texCoord;

out vec4 color;

uniform sampler2D texture1;
uniform sampler2D texture2;
uniform float mixValue;

void main()
{
    vec4 texColor2 = texture(texture2, texCoord);
    color = mix(texture(texture1, texCoord), texColor2, mixValue);
}