#version 330 core
out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} fs_in;

uniform vec3 albedo;
uniform float metallic;
uniform float roughness;
uniform float ao;

uniform vec3 camPos;

// lights
uniform vec3 lightPositions[4];
uniform vec3 lightColors[4];

vec3 BlinnPhongPointLight(vec3 normal, vec3 viewDir, vec3 lightPos, vec3 lightColor)
{
    vec3 ambient = vec3(0.03) * albedo;
    vec3 lightDir = normalize(lightPos - fs_in.FragPos);
    float diff = max(dot(normal, lightDir), 0.0);
    vec3 diffuse = diff * lightColor * albedo;

    vec3 halfDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(normal, halfDir), 0.0), 32);
    vec3 specular = spec * lightColor;

    return ambient + diffuse + specular;
}

void main()
{
    vec3 viewDir = normalize(camPos - fs_in.FragPos);
    vec3 normal = normalize(fs_in.Normal);

    vec3 color = vec3(0.0);
    for(int i = 0; i < 1; ++i) {
        color += BlinnPhongPointLight(normal, viewDir, lightPositions[i], lightColors[i]);
    }

    // HDR tonemapping
    color = color / (color + vec3(1.0));
    // gamma correct
    color = pow(color, vec3(1.0/2.2));

    FragColor = vec4(color, 1.0);
}